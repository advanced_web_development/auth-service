import { Injectable } from '@nestjs/common';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { promisify } from 'util';

const scrypt = promisify(_scrypt);
@Injectable()
export class HashPasswordService {
  async createHashPassword(password: String) {
    if (typeof password === 'string') {
      //Generate a salt
      const salt = randomBytes(8).toString('hex');
      //Hash the salt and the password together
      const hash = (await scrypt(password, salt, 32)) as Buffer;
      //Finally combie hash and salt
      return salt + '.' + hash.toString('hex');
    }
    return null;
  }
}
