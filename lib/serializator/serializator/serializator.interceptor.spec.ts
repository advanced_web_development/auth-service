import { SerializatorInterceptor } from './serializator.interceptor';

describe('SerializatorInterceptor', () => {
  it('should be defined', () => {
    expect(new SerializatorInterceptor()).toBeDefined();
  });
});
