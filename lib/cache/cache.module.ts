import { DynamicModule, Module } from '@nestjs/common';
import { CacheService } from './cache.service';

@Module({})
export class CacheModule {
  static register(cacheConfig: { host: string }): DynamicModule {
    return {
      module: CacheModule,
      providers: [
        {
          provide: 'CACHE_CONFIG',
          useValue: cacheConfig,
        },
        CacheService,
      ],
      exports: [CacheService],
    };
  }
}
