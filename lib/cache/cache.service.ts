import { Inject, Injectable } from '@nestjs/common';
import { RedisClientType, createClient } from 'redis';
import { config } from 'src/config';
@Injectable()
export class CacheService {
  private redis: RedisClientType;

  constructor(@Inject('CACHE_CONFIG') private cacheConfig: { host: string }) {
    this.redis = createClient({ url: cacheConfig.host });
    this.redis.connect();
  }

  getRedisClient(): RedisClientType {
    return this.redis;
  }
}
