import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CacheService } from 'lib/cache/cache.service';

interface decodedData {
  username: string;
  userId: number;
  age: number;
  email: string;
  first_name: string;
  last_name: string;
  login_time: string;
}

@Injectable()
export class SecurityGuard extends AuthGuard(['jwt']) {
  async canActivate(context: ExecutionContext): Promise<any> {
    try {
      // const request = context.switchToHttp().getRequest();
      // const { authorization }: any = request.headers;
      // if (!authorization || authorization.trim() === '') {
      //   throw new UnauthorizedException('Please provide token');
      // }
      // const authToken = authorization.replace(/bearer/gim, '').trim();

      // console.log(authToken);
      // const userData = await (await redis).get(authToken);
      // console.log(userData);
      // if (!userData) {
      //   throw new HttpException('UNAUTHORIZED', HttpStatus.UNAUTHORIZED);
      // }
      // request.userId = decodedToken.userId;
      // request.userKey = key;
      return super.canActivate(context);
    } catch (error) {
      throw new HttpException('UNAUTHORIZED', HttpStatus.UNAUTHORIZED);
    }
  }
}
