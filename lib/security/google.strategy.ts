import { Module } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, Profile } from 'passport-google-oauth20';
import { config } from 'src/config';

@Module({})
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor() {
    super({
      clientID: config.GOOGLE_APP_ID,
      clientSecret: config.GOOGLE_CLIENT_SECRET,
      callbackURL: config.GOOGLE_CALL_BACK_URL,
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email',
      ],
    });
  }
  async validate(
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    done: (err: any, user: any, info?: any) => void,
  ): Promise<any> {
    console.log('google authen');
    console.log(profile);

    const user = {
      email: profile._json.email,
      first_name: profile._json.given_name,
      last_name: profile._json.family_name,
      is_activated: profile._json.email_verified,
    };
    const payload = {
      user,
    };

    done(null, payload);
  }
}
