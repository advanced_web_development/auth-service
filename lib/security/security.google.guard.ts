import {
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class SecurityGoogleGuard extends AuthGuard(['google']) {
  async canActivate(context: ExecutionContext): Promise<any> {
    try {
      return super.canActivate(context);
    } catch (error) {
      throw new HttpException('UNAUTHORIZED', HttpStatus.UNAUTHORIZED);
    }
  }
}
