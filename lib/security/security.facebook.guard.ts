import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CacheService } from 'lib/cache/cache.service';
import { config } from 'src/config';

interface decodedData {
  username: string;
  userId: number;
  age: number;
  email: string;
  first_name: string;
  last_name: string;
  login_time: string;
}

@Injectable()
export class SecurityFacebookGuard extends AuthGuard(['facebook']) {
  async canActivate(context: ExecutionContext): Promise<any> {
    try {
      return super.canActivate(context);
    } catch (error) {
      throw new HttpException('UNAUTHORIZED', HttpStatus.UNAUTHORIZED);
    }
  }

  handleRequest(err: any, user: any, info: any, context: any, status: any) {
    const response = context.switchToHttp().getResponse();
    if (err || !user) {
      if (err.status === 500) {
        response.redirect(config.FAILURE_REDIRECT_URL);
      }
      throw new HttpException(err.message, err.status);
    }
    return user;
  }
}
