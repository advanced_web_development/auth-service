import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { accTokenPublicKey } from 'src/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: accTokenPublicKey,
    });
  }

  async validate(payload: any) {
    // console.log(payload);
    // if (payload.isActivated == false) {
    //   throw new HttpException(
    //     'Unauthorized! Your Email has not been confirmed yet',
    //     HttpStatus.UNAUTHORIZED,
    //   );
    // }
    return {
      userId: payload.userId,
      username: payload.username,
      email: payload.email,
    };
  }
}
