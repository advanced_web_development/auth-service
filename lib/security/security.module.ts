import { Module } from '@nestjs/common';
import { JwtStrategy } from './jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { FacebookStrategy } from './facebook.strategy';
import { RefreshTokenStrategy } from './refreshToken.strategy';
import { CacheModule } from 'lib/cache/cache.module';
import { GoogleStrategy } from './google.strategy';

@Module({
  imports: [PassportModule, CacheModule],
  providers: [
    JwtStrategy,
    FacebookStrategy,
    RefreshTokenStrategy,
    GoogleStrategy,
  ],
})
export class SecurityModule {}
