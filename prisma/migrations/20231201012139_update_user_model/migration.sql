-- AlterTable
ALTER TABLE "User" ADD COLUMN     "isActivate" BOOLEAN DEFAULT false,
ADD COLUMN     "last_login" TIMESTAMP(3),
ADD COLUMN     "phone_number" TEXT;
