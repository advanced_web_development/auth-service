// prisma/seed.ts

import { PrismaClient } from '@prisma/client';

// initialize Prisma Client
const prisma = new PrismaClient();

async function main() {
  // create two dummy articles
  const user1 = await prisma.user.create({
    data: {
      username: 'john_doe',
      first_name: 'John',
      last_name: 'Doe',
      password:
        'e61cf3ae634a4f82.d4939063cdf51957c8f6dd9cda849d803c4300b46c09cd97902a9aaaa6cefaab',
      birthdate: new Date('1985-07-22T08:45:00'),
      email: 'user1@example.com',
    },
  });

  const user2 = await prisma.user.create({
    data: {
      username: 'jane_smith',
      first_name: 'Jane',
      last_name: 'Smith',
      password:
        'e61cf3ae634a4f82.d4939063cdf51957c8f6dd9cda849d803c4300b46c09cd97902a9aaaa6cefaab',
      birthdate: new Date('2005-07-22T08:45:00'),
      email: 'user2@example.com',
    },
  });

  const user3 = await prisma.user.create({
    data: {
      username: 'bob_j',
      first_name: 'Bob',
      last_name: 'Johnson',
      password:
        'e61cf3ae634a4f82.d4939063cdf51957c8f6dd9cda849d803c4300b46c09cd97902a9aaaa6cefaab',
      birthdate: new Date('1990-07-22T08:45:00'),
      email: 'user3@example.com',
    },
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    // close Prisma Client at the end
    await prisma.$disconnect();
  });
