import { readFileSync } from 'fs';

require('dotenv').config();

class Config {
  DATABASE_URL: string = process.env.DATABASE_URL;
  TOKEN_EXPIRED_TIME: number =
    parseInt(process.env.TOKEN_EXPIRED_TIME) || 86400;
  CACHE_QUEUE_URL: string = process.env.CACHE_QUEUE_URL;
  FACEBOOK_APP_ID: string = process.env.FACEBOOK_APP_ID;
  FACEBOOK_APP_SECRET: string = process.env.FACEBOOK_APP_SECRET;
  FACEBOOK_CALL_BACK_URL: string = process.env.FACEBOOK_CALL_BACK_URL;
  ACCESS_TOKEN_EXPIRED_TIME: number = parseInt(
    process.env.ACCESS_TOKEN_EXPIRED_TIME,
  );
  REFRESH_TOKEN_EXPIRED_TIME: number = parseInt(
    process.env.REFRESH_TOKEN_EXPIRED_TIME,
  );
  FE_CALL_BACK_URL: string = process.env.FE_CALL_BACK_URL;
  GOOGLE_APP_ID: string = process.env.GOOGLE_APP_ID;
  GOOGLE_CLIENT_SECRET: string = process.env.GOOGLE_CLIENT_SECRET;
  GOOGLE_CALL_BACK_URL: string = process.env.GOOGLE_CALL_BACK_URL;
  MAIL_CONFIRMATION_REDIRECT: string = process.env.MAIL_CONFIRMATION_REDIRECT;
  MAIL_QUEUE_URL: string = process.env.MAIL_QUEUE_URL;
  MAIL_WORKER_URL: string = process.env.MAIL_WORKER_URL;
  MAIL_CONFIRMATION_SECRET_KEY: string =
    process.env.MAIL_CONFIRMATION_SECRET_KEY;
  FAILURE_REDIRECT_URL: string = process.env.FAILURE_REDIRECT_URL;
}
export const accTokenPrivateKey = readFileSync(
  './conf/accTokenKeys/private.key',
  'utf8',
);
export const accTokenPublicKey = readFileSync(
  './conf/accTokenKeys/public.key',
  'utf8',
);

export const refTokenPrivateKey = readFileSync(
  './conf/refTokenKeys/private.key',
  'utf8',
);
export const refTokenPublicKey = readFileSync(
  './conf/refTokenKeys/public.key',
  'utf8',
);
export const config = new Config();
