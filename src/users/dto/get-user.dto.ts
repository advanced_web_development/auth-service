import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsString, IsEmail, IsDate, IsBoolean } from 'class-validator';
export class GetUserDtoRes {
  @ApiProperty({
    default: '1',
  })
  @IsString()
  @Expose()
  id: number;

  @ApiProperty({
    default: 'user@gmail.com',
  })
  @IsString()
  @Expose()
  email: string;

  @ApiProperty({
    default: 'Alex',
  })
  @IsString()
  @Expose()
  first_name: string;

  @ApiProperty({
    default: 'NG',
  })
  @IsString()
  @Expose()
  last_name: string;

  @ApiProperty({})
  @Expose()
  birthdate: number;

  @ApiProperty({
    default: 'MyUsername',
  })
  @Expose()
  @IsString()
  username: string;

  @ApiProperty({
    default: false,
  })
  @IsBoolean()
  @Expose()
  is_activated: boolean;

  @ApiProperty()
  @IsString()
  @Expose()
  phone_number: string;
}
