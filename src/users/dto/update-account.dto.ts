import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class UpdateAccountUserDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    default: 'Username',
  })
  user_name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    default: 'Password',
  })
  password: string;
}
