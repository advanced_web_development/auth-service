import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class UpdatePasswordUserDto {
  @IsString()
  @ApiProperty({
    default: 'OldPassword',
  })
  old_password: string;

  @IsString()
  @ApiProperty({
    default: 'NewPassword',
  })
  new_password: string;
}
