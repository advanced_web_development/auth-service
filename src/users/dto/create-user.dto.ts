import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    default: 'user@gmail.com',
  })
  email: string;

  @ApiProperty({
    default: 'Andrew',
  })
  first_name: string;

  @ApiProperty({
    default: 'NG',
  })
  last_name: string;

  @ApiProperty({
    default: 1700310109,
  })
  birthdate: number;

  password: string;

  @ApiProperty({
    default: 'MyUsername',
  })
  username: string;
}
