import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { HashPasswordModule } from 'lib/hash-password/hash-password.module';

@Module({
  imports: [PrismaModule, HashPasswordModule],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
