import {
  Controller,
  Get,
  Body,
  Param,
  UseGuards,
  Put,
  Req,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { SecurityGuard } from 'lib/security/security.guard';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { Serialize } from 'lib/serializator/serializator/serializator.interceptor';
import { GetUserDtoRes } from './dto/get-user.dto';
import { UpdatePasswordUserDto } from './dto/update-password-user';
import { UpdateAccountUserDto } from './dto/update-account.dto';
@ApiTags('User')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiBearerAuth()
  @ApiCreatedResponse({ type: GetUserDtoRes })
  @Serialize(GetUserDtoRes)
  @UseGuards(SecurityGuard)
  @Get('/detail')
  findOne(@Req() request: Request) {
    const userIdReq = request['user'].userId;
    return this.usersService.findOne(userIdReq);
  }

  @ApiBearerAuth()
  @UseGuards(SecurityGuard)
  @Put('/update')
  update(@Body() updateUserDto: UpdateUserDto, @Req() request: Request) {
    const userIdReq = request['user'].userId;
    return this.usersService.update(updateUserDto, userIdReq);
  }

  @ApiBearerAuth()
  @UseGuards(SecurityGuard)
  @Put('/update_password')
  updatePassword(
    @Body() updatePasswordUser: UpdatePasswordUserDto,
    @Req() request: Request,
  ) {
    const userIdReq = request['user'].userId;
    return this.usersService.updatePassword(updatePasswordUser, userIdReq);
  }

  @ApiBearerAuth()
  @UseGuards(SecurityGuard)
  @Put('/update_account')
  updateAccount(
    @Body() updateAccountUser: UpdateAccountUserDto,
    @Req() request: Request,
  ) {
    const userIdReq = request['user'].userId;
    return this.usersService.updateAccount(updateAccountUser, userIdReq);
  }
}
