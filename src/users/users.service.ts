import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { UpdatePasswordUserDto } from './dto/update-password-user';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { promisify } from 'util';
import { HashPasswordService } from 'lib/hash-password/hash-password.service';
import { UpdateAccountUserDto } from './dto/update-account.dto';
const scrypt = promisify(_scrypt);

@Injectable()
export class UsersService {
  constructor(
    private prismaService: PrismaService,
    private hassPasswordService: HashPasswordService,
  ) {}

  async findOne(userIdReq: number) {
    const holdUser = await this.prismaService.user.findUnique({
      where: {
        id: userIdReq,
      },
    });
    const { password, isActivate, birthdate, ...userInfor } = holdUser;
    const _birthDate = birthdate ? birthdate.getTime() : null;
    // console.log(holdUser);
    return {
      ...userInfor,
      birthdate: _birthDate,
      is_activated: isActivate,
    };
  }

  async update(updateUserDto: UpdateUserDto, userIdReq: number) {
    let { birthdate, email, ...dataUser } = updateUserDto;
    let data: any;
    if (birthdate) {
      const _birthdate = new Date(updateUserDto.birthdate);
      data = { ...dataUser, birthdate: _birthdate };
    } else {
      data = { ...dataUser };
    }

    const holdUser = await this.prismaService.user.update({
      where: {
        id: userIdReq,
      },
      data: data,
    });
    const { password, ...userInfor } = holdUser;
    return {
      ...userInfor,
    };
  }
  async updatePassword(
    updatePasswordUser: UpdatePasswordUserDto,
    userIdReq: any,
  ) {
    const { old_password, new_password } = updatePasswordUser;

    //compare password
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userIdReq,
      },
    });
    const [oldSalt, storedHash] = user.password.split('.');
    const hashOldPassword = (await scrypt(old_password, oldSalt, 32)) as Buffer;
    if (storedHash === hashOldPassword.toString('hex')) {
    } else {
      throw new HttpException(
        'Invalid current password',
        HttpStatus.BAD_REQUEST,
      );
    }

    //hash new password
    const hashedPassword =
      await this.hassPasswordService.createHashPassword(new_password);
    //Update DTO
    const data = {
      password: hashedPassword,
    };

    const holdUser = await this.prismaService.user.update({
      where: {
        id: userIdReq,
      },
      data: data,
    });

    const { password, ...userInfor } = holdUser;
    return {
      ...userInfor,
    };
  }
  async updateAccount(
    updateAccountUserDto: UpdateAccountUserDto,
    userIdReq: any,
  ) {
    //check if username exists
    const userWithUsername = await this.prismaService.user.findUnique({
      where: {
        username: updateAccountUserDto.user_name,
      },
    });
    if (userWithUsername) {
      throw new HttpException(
        'Username has already existed',
        HttpStatus.BAD_REQUEST,
      );
    }

    const user = await this.prismaService.user.findUnique({
      where: {
        id: userIdReq,
      },
    });

    if (user.username !== null) {
      throw new HttpException(
        'The account has previously set up a username',
        HttpStatus.BAD_REQUEST,
      );
    }

    //hash new password
    const hashedPassword = await this.hassPasswordService.createHashPassword(
      updateAccountUserDto.password,
    );
    //Update DTO
    const data = {
      password: hashedPassword,
      username: updateAccountUserDto.user_name,
    };

    const holdUser = await this.prismaService.user.update({
      where: {
        id: userIdReq,
      },
      data: data,
    });

    const { password, ...userInfor } = holdUser;
    return {
      ...userInfor,
    };
  }
}
