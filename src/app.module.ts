import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { UsersModule } from './users/users.module';
import { SecurityModule } from 'lib/security/security.module';
import { HashPasswordModule } from 'lib/hash-password/hash-password.module';
import { BullModule } from '@nestjs/bull';
import { config } from './config';
import { MailProducerModule } from './mail-producer/mail-producer.module';

@Module({
  imports: [
    BullModule.forRoot({
      redis: config.MAIL_QUEUE_URL,
    }),
    AuthModule,
    PrismaModule,
    UsersModule,
    SecurityModule,
    HashPasswordModule,
    MailProducerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
