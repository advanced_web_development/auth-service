import { Module } from '@nestjs/common';

import { JwtAuthModule } from './jwt.auth/jwt.auth.module';
import { RouterModule } from '@nestjs/core';
import { FacebookModule } from './facebook/facebook.module';
import { GoogleModule } from './google/google.module';

@Module({
  imports: [
    JwtAuthModule,
    FacebookModule,
    GoogleModule,
    RouterModule.register([
      {
        path: 'auth',
        module: JwtAuthModule,
        children: [
          {
            path: 'jwt',
            module: JwtAuthModule,
          },
          {
            path: 'facebook',
            module: FacebookModule,
          },
          {
            path: 'google',
            module: GoogleModule,
          },
        ],
      },
    ]),
  ],
})
export class AuthModule {}
