import { IsBoolean, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
export class SignInDto {
  email: string;
  first_name: string;
  last_name: string;
  constructor(obj: { email: string; first_name: string; last_name: string }) {
    this.email = obj.email;
    this.first_name = obj.first_name;
    this.last_name = obj.last_name;
  }
}
export class SignInDtoRes {
  @ApiProperty()
  @IsString()
  @Expose()
  refresh_token: string;

  @ApiProperty()
  @IsString()
  @Expose()
  access_token: string;

  @ApiProperty()
  @IsString()
  @Expose()
  username: string;

  @ApiProperty()
  @IsString()
  @Expose()
  email: string;

  @ApiProperty()
  @IsString()
  @Expose()
  birthdate: string;

  @ApiProperty()
  @IsString()
  @Expose()
  first_name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  last_name: string;

  @ApiProperty()
  @IsString()
  @Expose()
  user_id: string;

  @ApiProperty()
  @IsBoolean()
  @Expose()
  is_activated: boolean;

  @ApiProperty()
  @IsString()
  @Expose()
  phone_number: string;
}
