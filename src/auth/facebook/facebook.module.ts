import { Module } from '@nestjs/common';
import { FacebookService } from './facebook.service';
import { FacebookController } from './facebook.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtAuthModule } from '../jwt.auth/jwt.auth.module';

@Module({
  providers: [FacebookService],
  controllers: [FacebookController],
  imports: [PrismaModule, JwtAuthModule],
  exports: [FacebookService],
})
export class FacebookModule {}
