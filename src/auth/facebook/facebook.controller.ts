import {
  Controller,
  Get,
  HttpStatus,
  Redirect,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiOAuth2, ApiTags } from '@nestjs/swagger';
import { FacebookService } from './facebook.service';
import { SignInDto } from './dtos/signIn.dto';
import { config } from 'src/config';
import { SecurityFacebookGuard } from 'lib/security/security.facebook.guard';

@ApiTags('Facebook Auth')
@Controller()
export class FacebookController {
  constructor(private readonly facebookService: FacebookService) {}
  @Get('/')
  @UseGuards(SecurityFacebookGuard)
  async facebookLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('/redirect')
  @UseGuards(SecurityFacebookGuard)
  @Redirect(config.FE_CALL_BACK_URL)
  async facebookLoginRedirect(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    // return {
    //   statusCode: HttpStatus.OK,
    //   data: req['user'],
    // };
    const user = req['user'].user;
    const payload = await this.facebookService.signIn(new SignInDto(user));
    // res.cookie('auth', JSON.stringify(payload));
    return { url: config.FE_CALL_BACK_URL + '/' + JSON.stringify(payload) };
  }

  @Get('/test')
  @UseGuards(SecurityFacebookGuard)
  async testFb(@Req() req: Request): Promise<any> {
    console.log(req['user']);
    return {
      ok: 'ok',
    };
  }
}
