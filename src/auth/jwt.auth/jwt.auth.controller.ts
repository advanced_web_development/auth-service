import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  Put,
  Query,
  Redirect,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthService } from './jwt.auth.service';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { Serialize } from 'lib/serializator/serializator/serializator.interceptor';
import { SecurityGuard } from 'lib/security/security.guard';
import { SignUpDto } from './dtos/signUp.dto';
import { SignInDto, SignInDtoRes } from './dtos/signIn.dto';
import { RefreshTokenGuard } from 'lib/security/refreshToken.guard';
import { config } from 'src/config';
import { Response } from 'express';
import { MailConfirmDto } from './dtos/mailConfirm.dto';
import {
  ForgetPasswordDto,
  UpdatePasswordDto,
  VerifyCodeDto,
} from './dtos/forgetPassword.dto';

@ApiTags('JWT Auth')
@Controller()
export class JwtAuthController {
  constructor(private readonly jwtAuthService: JwtAuthService) {}

  @Post('/sign_up')
  async signUp(@Body() signUpDto: SignUpDto) {
    const result = await this.jwtAuthService.signUp(signUpDto);
    await this.jwtAuthService.confirmMail(signUpDto.email);
    return result;
  }

  @ApiCreatedResponse({ type: SignInDtoRes })
  @Serialize(SignInDtoRes)
  @Post('/sign_in')
  @HttpCode(203)
  signIn(@Body() signInDto: SignInDto) {
    return this.jwtAuthService.signIn(signInDto);
  }

  @ApiBearerAuth()
  @UseGuards(SecurityGuard)
  @Post('sign_out')
  signOut(@Req() request: Request) {
    const userId: number = request['user'].userId;
    return this.jwtAuthService.signOut(userId.toString());
    // return user;
  }

  @ApiBearerAuth()
  @Post('refresh')
  @UseGuards(RefreshTokenGuard)
  refresh(@Req() req: Request) {
    const userId: number = req['user'].userId;
    const refresh_token = req['user'].refresh_token;

    return this.jwtAuthService.refreshTokens(userId, refresh_token);
  }

  @Get('confirm/redirect')
  @Redirect(config.MAIL_CONFIRMATION_REDIRECT)
  confirmMailRedirect(
    @Query('token') token: string,
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    // res.cookie('mail_confirmation_token', token, {
    //   httpOnly: true,
    //   sameSite: 'none',
    //   secure: true,
    // });
    return {
      url: `${config.MAIL_CONFIRMATION_REDIRECT}/${token}`,
    };
  }

  @Put('confirm')
  confirmMail(@Body() mailConfirmationDto: MailConfirmDto) {
    return this.jwtAuthService.verifyMailConfirmation(
      mailConfirmationDto.token,
    );
  }

  @ApiBearerAuth()
  @Post('resend_email_confirmation')
  @UseGuards(SecurityGuard)
  async resendConfirmationEmail(@Req() req: Request) {
    const email = req['user'].email;
    const result = await this.jwtAuthService.resendMailConfirmation(email);
    return result;
  }

  @Post('forgot_password')
  @HttpCode(200)
  async forgetPassword(@Body() forgetPasswordDto: ForgetPasswordDto) {
    const username = forgetPasswordDto.username;
    return await this.jwtAuthService.forgetPassword(username);
  }
  @Post('verify_code')
  @HttpCode(200)
  async verifyCode(@Body() verifyCodeDto: VerifyCodeDto) {
    const username = verifyCodeDto.username;
    const verificationCode = verifyCodeDto.verify_code;
    return this.jwtAuthService.verifyCode(
      username,
      verificationCode.toString(),
    );
  }
  @Post('update_password')
  @HttpCode(200)
  async updatePassword(@Body() updatePasswordDto: UpdatePasswordDto) {
    const username = updatePasswordDto.username;
    const verification_code = updatePasswordDto.verify_code;
    const new_password = updatePasswordDto.password;

    return await this.jwtAuthService.updatePassword(
      username,
      verification_code,
      new_password,
    );
  }
}
