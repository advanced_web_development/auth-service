import { Module } from '@nestjs/common';
import { JwtAuthController } from './jwt.auth.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { config, accTokenPrivateKey } from 'src/config';
import { JwtAuthService } from './jwt.auth.service';
import { CacheModule } from 'lib/cache/cache.module';
import { HashPasswordModule } from 'lib/hash-password/hash-password.module';
import { BullModule } from '@nestjs/bull';
import { HttpModule } from '@nestjs/axios';
import { MailProducerModule } from 'src/mail-producer/mail-producer.module';
@Module({
  controllers: [JwtAuthController],
  imports: [
    HttpModule,
    MailProducerModule,
    PrismaModule,
    CacheModule.register({
      host: config.CACHE_QUEUE_URL,
    }),
    HashPasswordModule,
    JwtModule,
  ],
  providers: [JwtAuthService],
  exports: [JwtAuthService],
})
export class JwtAuthModule {}
