import {
  BadRequestException,
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  Req,
} from '@nestjs/common';

import { SignUpDto } from './dtos/signUp.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { SignInDto, SignInDtoRes } from './dtos/signIn.dto';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { promisify } from 'util';
import {
  config,
  refTokenPublicKey,
  refTokenPrivateKey,
  accTokenPrivateKey,
} from 'src/config';

import { JwtService } from '@nestjs/jwt';
import { UserData } from './interfaces/UserData';

import { CacheService } from 'lib/cache/cache.service';
import { User } from '@prisma/client';
import { HashPasswordService } from 'lib/hash-password/hash-password.service';
import { MailProducerService } from 'src/mail-producer/mail-producer.service';

const scrypt = promisify(_scrypt);
@Injectable()
export class JwtAuthService {
  constructor(
    private prismaService: PrismaService,
    private jwtService: JwtService,
    private cacheService: CacheService,
    private hassPasswordService: HashPasswordService,
    private mailProducerService: MailProducerService,
  ) {}

  async signUp(signUpDto: SignUpDto) {
    if (signUpDto.username) {
      const _username = signUpDto.username;
      const _email = signUpDto.email;
      //Check the username if it existed in the database
      const user = await this.prismaService.user.findUnique({
        where: {
          username: _username,
        },
      });
      //If it does then throw error
      if (user !== null) {
        throw new HttpException(
          'Username already existed',
          HttpStatus.BAD_REQUEST,
        );
      }

      //check if email existed in the database
      const isEmailExisted = await this.prismaService.user.findUnique({
        where: {
          email: _email,
        },
      });
      //if it does then throw error
      if (isEmailExisted) {
        throw new HttpException(
          'Email is already existed',
          HttpStatus.BAD_GATEWAY,
        );
      }
    }

    //Else create new user
    //First hash the user password
    const _password = signUpDto.password;
    //hash password
    const hashedPassword =
      await this.hassPasswordService.createHashPassword(_password);

    const _data = {
      email: signUpDto.email,
      birthdate: signUpDto.birthdate ? new Date(signUpDto.birthdate) : null,
      password: hashedPassword,
      first_name: signUpDto.first_name || null,
      last_name: signUpDto.last_name || null,
      username: signUpDto.username || null,
      phone_number: signUpDto.phone_number || null,
    };
    const newUser = await this.prismaService.user.create({ data: _data });
    // console.log(newUser);
    return {
      statusCode: 201,
      message: 'Register account successfully!',
    };
  }

  async signIn(signInDto: SignInDto) {
    const _password = signInDto.password;
    const _username = signInDto.username;

    const user = await this.prismaService.user.findUnique({
      where: {
        username: _username,
      },
    });

    if (!user) {
      throw new HttpException('Invalid username', HttpStatus.UNAUTHORIZED);
    }
    const [salt, storedHash] = user.password.split('.');

    const hash = (await scrypt(_password, salt, 32)) as Buffer;

    if (storedHash === hash.toString('hex')) {
    } else {
      throw new HttpException('Auth failure error', HttpStatus.UNAUTHORIZED);
    }
    const payload = this.prepareUserDataPayload(user);
    const [access_token, refresh_token] = this.createToken(payload);

    //cached the refresh token
    await this.cacheRefreshToken(user.id, refresh_token);

    return {
      access_token: access_token,
      refresh_token: refresh_token,
      username: user.username,
      first_name: user.first_name,
      last_name: user.last_name,
      user_id: user.id,
      is_activated: user.isActivate,
      phone_number: user.phone_number,
      email: user.email,
    };
  }

  async signOut(user_id: string) {
    const cache = this.cacheService.getRedisClient();
    const isLoggedIn = await cache.get(user_id);
    if (isLoggedIn == null) {
      throw new HttpException(
        'You are already logged out',
        HttpStatus.BAD_REQUEST,
      );
    }
    await cache.del(user_id);
    return {
      statusCode: 200,
      message: 'Sign out succesfully',
    };
  }
  prepareUserDataPayload(user: User): UserData {
    const loginTime = new Date().getTime();
    const payload = {
      userId: user.id,
      username: user.username,
      email: user.email,
      birthdate: user.birthdate,
      first_name: user.first_name,
      last_name: user.last_name,
      login_time: loginTime,
      isActivated: user.isActivate,
    };
    return payload;
  }
  async cacheRefreshToken(userId: number, refresh_token: string) {
    this.cacheService
      .getRedisClient()
      .set(userId.toString(), refresh_token, { EX: config.TOKEN_EXPIRED_TIME });
  }
  async refreshTokens(
    userId: number,
    refreshToken: string,
  ): Promise<{ access_token: string; refresh_token: string }> {
    const cachedRefToken = await this.cacheService
      .getRedisClient()
      .get(userId.toString());
    const user = await this.prismaService.user.findUnique({
      where: {
        id: userId,
      },
    });
    if (!user || cachedRefToken !== refreshToken) {
      throw new ForbiddenException('Access Denined');
    }
    const payload = this.prepareUserDataPayload(user);
    const [access_token, refresh_token] = this.createToken(payload);
    await this.cacheRefreshToken(user.id, refresh_token);
    return {
      access_token: access_token,
      refresh_token: refresh_token,
    };
  }
  createToken(user: UserData): [string, string] {
    const payload = user;
    const access_token = this.createAccessToken(payload);
    const refresh_token = this.createRefreshToken(payload);
    return [access_token, refresh_token];
  }

  createAccessToken(user: UserData) {
    const payload = user;
    const refresh_token = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Refresh',
      expiresIn: config.ACCESS_TOKEN_EXPIRED_TIME,
      algorithm: 'RS256',
      privateKey: accTokenPrivateKey,
    });

    return refresh_token;
  }
  createRefreshToken(user: UserData) {
    const payload = user;
    const refresh_token = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Refresh',
      expiresIn: config.REFRESH_TOKEN_EXPIRED_TIME,
      algorithm: 'RS256',
      privateKey: refTokenPrivateKey,
    });

    return refresh_token;
  }

  async confirmMail(email: string) {
    const payload = { email: email };
    const token = this.jwtService.sign(payload, {
      issuer: 'RoadToGraduation',
      subject: 'Mail Confirmation',
      expiresIn: config.REFRESH_TOKEN_EXPIRED_TIME,
      algorithm: 'HS256',
      secret: config.MAIL_CONFIRMATION_SECRET_KEY,
    });

    // console.log(token);
    // const job = await this.mailQueue.add('mail_confirmation', {
    //   email: email,
    //   token: token,
    // });
    const _payload = {
      email: email,
      token: token,
    };
    await this.mailProducerService.confirmMail(_payload);
  }

  async resendMailConfirmation(email: string) {
    const user = await this.prismaService.user.findUnique({
      where: {
        email: email,
      },
    });
    if (user.isActivate === true) {
      throw new HttpException(
        'Your email has already been activated',
        HttpStatus.FORBIDDEN,
      );
    }
    await this.confirmMail(email);
    return {
      statusCode: 200,
      message: 'Successfully resend the email confirmation!',
    };
  }

  async verifyMailConfirmation(token: string) {
    let result: { email: string; iat: number; exp: number };
    try {
      result = this.jwtService.verify(token, {
        secret: config.MAIL_CONFIRMATION_SECRET_KEY,
      });
    } catch (error) {
      // console.log(error);
      throw new HttpException(
        'Token provided is invalid',
        HttpStatus.FORBIDDEN,
      );
    }
    const email = result.email;
    const user = await this.prismaService.user.findUnique({
      where: {
        email: email,
      },
    });
    if (user.isActivate === true) {
      throw new HttpException(
        'Your email has already been activated',
        HttpStatus.FORBIDDEN,
      );
    }
    const _result = await this.prismaService.user.update({
      where: {
        email: email,
      },
      data: {
        isActivate: true,
      },
    });

    if (!user) {
      throw new HttpException('Cant verify your email', HttpStatus.FORBIDDEN);
    }

    return {
      statusCode: 200,
      message: 'Update successfully',
    };
  }

  async forgetPassword(username: string) {
    const user = await this.prismaService.user.findUnique({
      where: {
        username: username,
      },
    });
    if (!user) {
      throw new HttpException('Username not existed', HttpStatus.NOT_FOUND);
    }
    const email = user.email;
    const randomCode = Math.floor(Math.random() * 9000 + 1000);

    // console.log(randomCode);
    const payload = {
      email: email,
      code: randomCode,
    };
    const redisKey = `${user.id}.resetpasswordcode`;
    await this.cacheService.getRedisClient().set(redisKey, randomCode, {
      EX: 3600,
    });
    // const job = await this.mailQueue.add('forgot_password', payload);
    await this.mailProducerService.forgotPassword(payload);
    return {
      username: user.username,
      email: user.email,
    };
  }

  async checkKey(username: string, key: string): Promise<User> {
    const user = await this.prismaService.user.findUnique({
      where: {
        username: username,
      },
    });
    if (!username) {
      throw new HttpException('Username not existed', HttpStatus.NOT_FOUND);
    }
    const redisKey = `${user.id}.resetpasswordcode`;
    const code = await this.cacheService.getRedisClient().get(redisKey);

    if (code !== key) {
      throw new HttpException('Wrong verifcation code', HttpStatus.CONFLICT);
    }

    return user;
  }

  async verifyCode(username: string, key: string) {
    const user = await this.checkKey(username, key);
    if (!user) {
      throw new HttpException('Wrong verification code', HttpStatus.NOT_FOUND);
    }
    return {
      statusCode: 200,
      message: 'This code is valid',
    };
  }
  async updatePassword(
    username: string,
    verifcationCode: number,
    newPassword: string,
  ) {
    const user = await this.checkKey(username, verifcationCode.toString());

    const redisKey = `${user.id}.resetpasswordcode`;
    const hashedPassword =
      await this.hassPasswordService.createHashPassword(newPassword);
    await this.prismaService.user.update({
      where: {
        username: user.username,
      },
      data: {
        password: hashedPassword,
      },
    });
    await this.cacheService.getRedisClient().del(redisKey);

    return {
      statusCode: 200,
      message: 'Update new password successully!',
    };
  }
}
