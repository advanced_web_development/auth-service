import { IsEmail, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ForgetPasswordDto {
  @ApiProperty({
    default: 'myUsername',
  })
  @IsString()
  username: string;
}

export class UpdatePasswordDto {
  @ApiProperty({
    default: 'myUsername',
  })
  @IsString()
  username: string;

  @ApiProperty({
    default: 'myNewPassword',
  })
  @IsString()
  password: string;

  @ApiProperty({
    default: 1234,
    description: 'Please check your email to get the verification code',
  })
  @IsNumber()
  verify_code: number;
}

export class VerifyCodeDto {
  @ApiProperty({
    default: 'myUsername',
  })
  @IsString()
  username: string;

  @ApiProperty({
    default: 1234,
    description: 'Please check your email to get the verification code',
  })
  @IsNumber()
  verify_code: number;
}
