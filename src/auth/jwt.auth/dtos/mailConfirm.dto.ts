import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class MailConfirmDto {
  @ApiProperty({
    default: 'Your Token',
  })
  @IsString()
  token: string;
}

export class ResendEmailConfirmation {
  @ApiProperty({
    default: 'youemail@email.com',
  })
  @IsEmail()
  email: string;
}
