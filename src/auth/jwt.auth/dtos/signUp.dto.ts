import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEmail,
  IsNotEmpty,
  IsDateString,
  IsInt,
} from 'class-validator';

export class SignUpDto {
  @ApiProperty({
    default: 'user@gmail.com',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({
    default: 'MyUsername',
  })
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({
    default: 'Andrew',
  })
  @IsString()
  first_name: string;

  @ApiProperty({
    default: 'Nguyen',
  })
  @IsString()
  last_name: string;

  @ApiProperty({
    description: 'Please enter a valid TimeStamp, Ex: 1700280869',
    default: 1700280869,
  })
  @IsInt()
  birthdate: number;

  @ApiProperty({
    default: 'MyPassword',
  })
  @IsNotEmpty()
  @IsString()
  password: string;

  @ApiProperty({
    default: '012345678',
  })
  @IsString()
  phone_number: string;
}
