export interface UserData {
  username: string;
  userId: number;
  birthdate: Date;
  email: string;
  first_name: string;
  last_name: string;
}
