import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtAuthService } from '../jwt.auth/jwt.auth.service';
import { SignInDto } from './dtos/signIn.dto';

@Injectable()
export class GoogleService {
  constructor(
    private prismaService: PrismaService,
    private jwtAuthService: JwtAuthService,
  ) {}

  async signIn(signInDto: SignInDto) {
    const _email = signInDto.email;
    const user = await this.prismaService.user.findUnique({
      where: {
        email: _email,
      },
    });
    if (user !== null) {
      //user exists
      // if (
      //   user.isActivate !== signInDto.is_activated &&
      //   signInDto.is_activated === true
      // ) {
      //   await this.prismaService.user.update({
      //     where: {
      //       email: _email,
      //     },
      //     data: {
      //       isActivate: signInDto.is_activated,
      //     },
      //   });
      //   user.isActivate = true;
      // }

      if (user.isActivate === false) {
        await this.prismaService.user.update({
          where: {
            email: _email,
          },
          data: {
            isActivate: true,
          },
        });
        user.isActivate = true;
      }

      const payload = this.jwtAuthService.prepareUserDataPayload(user);
      const [access_token, refresh_token] =
        this.jwtAuthService.createToken(payload);

      await this.jwtAuthService.cacheRefreshToken(user.id, refresh_token);

      return {
        access_token: access_token,
        refresh_token: refresh_token,
        username: user.username,
        first_name: user.first_name,
        last_name: user.last_name,
        user_id: user.id,
        is_activated: user.isActivate,
        phone_number: user.phone_number,
        email: user.email,
      };
    } else {
      //create new user
      const _data = {
        email: signInDto.email,
        birthdate: null,
        password: null,
        first_name: signInDto.first_name || null,
        last_name: signInDto.last_name || null,
        isActivate: true,
        username: null,
        phone_number: null,
      };
      const newUser = await this.prismaService.user.create({ data: _data });

      //handle return
      const payload = this.jwtAuthService.prepareUserDataPayload(newUser);
      const [access_token, refresh_token] =
        this.jwtAuthService.createToken(payload);

      await this.jwtAuthService.cacheRefreshToken(newUser.id, refresh_token);

      return {
        access_token: access_token,
        refresh_token: refresh_token,
        username: newUser.username,
        first_name: newUser.first_name,
        last_name: newUser.last_name,
        user_id: newUser.id,
        is_active: newUser.isActivate,
        phone_number: newUser.phone_number,
        email: newUser.email,
      };
    }
  }
}
