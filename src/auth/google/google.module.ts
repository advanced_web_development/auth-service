import { Module } from '@nestjs/common';
import { GoogleController } from './google.controller';
import { GoogleService } from './google.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtAuthModule } from '../jwt.auth/jwt.auth.module';

@Module({
  controllers: [GoogleController],
  providers: [GoogleService],
  exports: [GoogleService],
  imports: [PrismaModule, JwtAuthModule],
})
export class GoogleModule {}
