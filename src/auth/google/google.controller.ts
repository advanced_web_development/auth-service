import {
  Controller,
  Get,
  HttpStatus,
  Redirect,
  UseGuards,
  Req,
  Res,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SecurityGoogleGuard } from 'lib/security/security.google.guard';
import { config } from 'src/config';
import { GoogleService } from './google.service';
import { SignInDto } from './dtos/signIn.dto';
import { Response } from 'express';

@ApiTags('Google Auth')
@Controller()
export class GoogleController {
  constructor(private readonly googleService: GoogleService) {}
  @Get('/')
  @UseGuards(SecurityGoogleGuard)
  async googleLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('/callback')
  @UseGuards(SecurityGoogleGuard)
  @Redirect(config.FE_CALL_BACK_URL)
  async googleLoginCallBack(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ): Promise<any> {
    const user = req['user'].user;
    const payload = await this.googleService.signIn(new SignInDto(user));
    // res.cookie('auth', JSON.stringify(payload));
    return { url: config.FE_CALL_BACK_URL + '/' + JSON.stringify(payload) };
  }

  @Get('/test')
  @UseGuards(SecurityGoogleGuard)
  async testFb(@Req() req: Request): Promise<any> {
    console.log(req['user']);
    return {
      ok: 'ok',
    };
  }
}
