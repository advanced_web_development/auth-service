import { Controller, Get } from '@nestjs/common';
import { MailProducerService } from './mail-producer.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Mail Producer')
@Controller('mail-producer')
export class MailProducerController {
  constructor(private mailProducerService: MailProducerService) {}
  @Get()
  test() {
    this.mailProducerService.confirmMail({ email: 'haha', token: 'tiki' });
  }
}
