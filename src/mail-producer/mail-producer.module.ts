import { Module } from '@nestjs/common';
import { MailProducerService } from './mail-producer.service';
import { HttpModule } from '@nestjs/axios';
import { BullModule } from '@nestjs/bull';
import { CacheModule } from 'lib/cache/cache.module';
import { MailProducerController } from './mail-producer.controller';
import { config } from 'src/config';

@Module({
  imports: [
    CacheModule.register({
      host: config.CACHE_QUEUE_URL,
    }),
    HttpModule.register({ timeout: 5000, maxRedirects: 5 }),
    BullModule.registerQueue({
      name: 'mail',
    }),
  ],
  providers: [MailProducerService],
  controllers: [MailProducerController],
  exports: [MailProducerService],
})
export class MailProducerModule {}
