import { HttpService } from '@nestjs/axios';
import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { Queue } from 'bull';
import { CacheService } from 'lib/cache/cache.service';
import { config } from 'src/config';

@Injectable()
export class MailProducerService {
  constructor(
    private cacheService: CacheService,
    private httpService: HttpService,
    @InjectQueue('mail') private mailQueue: Queue,
  ) {}

  async confirmMail(payload: { email: string; token: string }) {
    const result = await this.httpService.axiosRef.get(config.MAIL_WORKER_URL);
    console.log(result.status, result.data);
    const job = await this.mailQueue.add('mail_confirmation', payload);
  }

  async forgotPassword(payload: { email: string; code: number }) {
    const result = await this.httpService.axiosRef.get(config.MAIL_WORKER_URL);
    console.log(result.status, result.data);
    const job = await this.mailQueue.add('forgot_password', payload);
  }
}
