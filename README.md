# Environment setup

## 1. Install all node modules.

Open the terminal in the source code folder and run the following command to install all the necessary node modules:

“npm i”

## 2. Database and redis init (optional).

We have prepared a Docker Compose file for the project. To create containers for PostgreSQL and Redis, run the following command:

“docker compose up”

If you prefer to test the source code with your own PostgreSQL and Redis, you can skip this step.

## 3. Create .env file

The source code requires a .env file to store secret data. Create a .env file in the source code folder with the following content:

DATABASE_URL="postgres://myuser:mypassword@localhost:5433/my-db"
EXPIRED_TIME=300
ACCESS_TOKEN_EXPIRED_TIME=300
REFRESH_TOKEN_EXPIRED_TIME=604800
CACHE_QUEUE_URL="redis://default:eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81@localhost:6380"
FACEBOOK_APP_ID=1027061228598720
FACEBOOK_APP_SECRET=0630f424464c56730a44c1725a0ebecc
FACEBOOK_CALL_BACK_URL=http://localhost:3000/auth/facebook/redirect
MAIL_CONFIRMATION_REDIRECT=https://www.youtube.com/
GOOGLE_APP_ID=1027061228598720
GOOGLE_CLIENT_SECRET=0630f424464c56730a44c1725a0ebecc
GOOGLE_CALL_BACK_URL=http://localhost:3000/facebook/redirect
MAIL_QUEUE_URL=redis://default:eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81@localhost:6379
FE_CALL_BACK_URL=https://ga01.netlify.app/user/setup
MAIL_WORKER_URL=https://mail-service.adaptable.app/
MAIL_CONFIRMATION_SECRET_KEY=RTG

Note that these values are based on the database and redis that we create via docker compose in section 2.
Note: These values are based on the database and Redis created via Docker Compose in section 2. If testing on a deployed database, use the provided alternative values. You can also replace them with your desired values.

## 4. Prisma

### Prisma migrate DB

If you want to automatically generate the database, run the following command and confirm with "y":

“npx prisma migrate dev”

### Prisma seed init

In order to create ready dummy data please run npx

" npx prisma db seed"

### Alternatively, if you want to connect to an existing database, run:

“npx prisma introspect” or "npx prisma db pull"

## 5. Start the project

Run “npx nest start” to start the project.

## 6. Swagger.

We have created a quick document for testing the API. Access it by redirecting to the "/api" route.

The script will automatically generate User table and some dummy data for you!
